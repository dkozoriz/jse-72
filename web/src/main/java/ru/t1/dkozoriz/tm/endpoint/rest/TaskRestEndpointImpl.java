package ru.t1.dkozoriz.tm.endpoint.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.t1.dkozoriz.tm.api.TaskEndpoint;
import ru.t1.dkozoriz.tm.model.Task;
import ru.t1.dkozoriz.tm.service.TaskService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/task")
public class TaskRestEndpointImpl implements TaskEndpoint {

    private final TaskService taskService;

    @Override
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/getAll")
    public List<Task> getAll() {
        return taskService.findAll();
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/count")
    public Long count() {
        return taskService.count();
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/get/{id}")
    public Task get(
            @PathVariable("id") String id
    ) {
        return taskService.findById(id);
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    @PostMapping("/post")
    public Task post(
            @RequestBody Task task
    ) {
        return taskService.save(task);
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    @PutMapping("/put")
    public Task put(
            @RequestBody Task task
    ) {
        return taskService.update(task);
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    @DeleteMapping("/delete/{id}")
    public void delete(
            @PathVariable("id") String id
    ) {
        taskService.deleteById(id);
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    @DeleteMapping("/deleteAll")
    public void deleteAll() {
        taskService.deleteAll();
    }

}