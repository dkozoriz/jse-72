package ru.t1.dkozoriz.unit.repository;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.dkozoriz.marker.UnitCategory;
import ru.t1.dkozoriz.tm.configuration.DataConfiguration;
import ru.t1.dkozoriz.tm.model.Task;
import ru.t1.dkozoriz.tm.repository.TaskRepository;
import ru.t1.dkozoriz.tm.util.UserUtil;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataConfiguration.class})
@Category(UnitCategory.class)
public class TaskRepositoryTest {

    private final Task task1 = new Task("Test Task 1", "description 1");

    private final Task task2 = new Task("Test Task 2", "description 2");

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    public void initTest() {
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test_user", "test_user");
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        task1.setUserId(UserUtil.getUserId());
        task2.setUserId(UserUtil.getUserId());
        taskRepository.save(task1);
        taskRepository.save(task2);
    }

    @After
    public void clean() {
        taskRepository.deleteByUserId(UserUtil.getUserId());
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(2, taskRepository.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    public void findByIdTest() {
        final String taskId = task1.getId();
        final Task task = taskRepository.findByUserIdAndId(UserUtil.getUserId(), taskId);
        Assert.assertEquals(task.getId(), task1.getId());
        Assert.assertEquals(task.getName(), task1.getName());
        Assert.assertEquals(task.getDescription(), task1.getDescription());
        Assert.assertEquals(task.getStatus(), task1.getStatus());
        Assert.assertEquals(task.getUserId(), task1.getUserId());
        Assert.assertEquals(task.getCreated(), task1.getCreated());
    }

    @Test
    public void countTest() {
        final long count = taskRepository.countByUserId(UserUtil.getUserId());
        Assert.assertEquals(count, taskRepository.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    public void deleteByIdTest() {
        final String taskId = task1.getId();
        taskRepository.deleteByUserIdAndId(UserUtil.getUserId(), taskId);
        Assert.assertEquals(1, taskRepository.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    public void deleteAllTest() {
        taskRepository.deleteByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, taskRepository.findAllByUserId(UserUtil.getUserId()).size());
    }

}