package ru.t1.dkozoriz.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.dkozoriz.tm.api.endpoint.IAuthEndpoint;
import ru.t1.dkozoriz.tm.api.service.IAuthService;
import ru.t1.dkozoriz.tm.api.service.dto.IUserDtoService;
import ru.t1.dkozoriz.tm.dto.model.SessionDto;
import ru.t1.dkozoriz.tm.dto.model.UserDto;
import ru.t1.dkozoriz.tm.dto.request.user.UserLoginRequest;
import ru.t1.dkozoriz.tm.dto.request.user.UserLogoutRequest;
import ru.t1.dkozoriz.tm.dto.request.user.UserViewProfileRequest;
import ru.t1.dkozoriz.tm.dto.response.user.LoginUserResponse;
import ru.t1.dkozoriz.tm.dto.response.user.LogoutUserResponse;
import ru.t1.dkozoriz.tm.dto.response.user.ViewProfileUserResponse;
import ru.t1.dkozoriz.tm.exception.EndpointException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@WebService(endpointInterface = "ru.t1.dkozoriz.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    @Autowired
    private IAuthService authService;

    @Autowired
    private IUserDtoService userService;

    @NotNull
    @Override
    @WebMethod
    public LoginUserResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLoginRequest request
    ) {
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @NotNull final String token;
        try {
            token = authService.login(login, password);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new LoginUserResponse(token);
    }

    @NotNull
    @Override
    @WebMethod
    public LogoutUserResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLogoutRequest request
    ) {
        @NotNull final SessionDto session = checkPermission(request);
        try {
            authService.invalidate(session);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new LogoutUserResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ViewProfileUserResponse getProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserViewProfileRequest request
    ) {
        @NotNull final SessionDto session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @NotNull final UserDto user;
        try {
            user = userService.findById(userId);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new ViewProfileUserResponse(user);
    }

}