package ru.t1.dkozoriz.tm.api.service.model.business;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.model.business.Project;

public interface IProjectService extends IBusinessService<Project> {

    @NotNull Project create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

}