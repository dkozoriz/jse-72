package ru.t1.dkozoriz.tm.dto.response.project;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.model.business.ProjectDto;
import ru.t1.dkozoriz.tm.dto.response.AbstractResponse;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StartProjectByIndexResponse extends AbstractResponse {

    @Nullable
    private ProjectDto project;

}